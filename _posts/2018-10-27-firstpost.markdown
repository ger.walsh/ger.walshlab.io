---
layout: post
title:  "Achievement unlocked: Blog-starter"
date:   2017-10-27 20:00:00 -0100
categories: update
published: true
---
Unprecedented procrastination now has me investigating web-suitable video codecs, and html generator tools and presentation formats.

[big](https://github.com/tmcw/big) by [Tom MacWright](https://macwright.org/about/) is an awesome thing I found - I recommend checking it out.

[Check this daecent example out](../../../../collection_presentations/hello/index.html)

The work I am avoiding doing is supposed to go [here](#).
