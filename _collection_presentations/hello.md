---
title: "Hello: a short presentation"
filename: "hello"
date: 2018-10-29 10:00:00 +0000
published: true
presentation: /hello/index.html
description: "Test Integration with RevealJS in Jekyll"
separator: "\n---\n"
verticalSeparator: "^\n\n"
theme: simple
revealOptions:
    transition: 'none'
    # can be  none/fade/slide/convex/concave/zoom
    controls: false
    progress: false
---
Hello,
---
Pretty busy at the moment, eh? Lotsa deadlines?
---
Don't worry about that right now. Here's a nice video of a dog.
---
<!-- .slide: data-background-video="http://academy.cba.mit.edu/classes/computer_design/antimony.mp4" data-background-video-loop="1" -->
<span style="color:white"> **OOPS...** </span>
<span style="color:grey">**Must've typed "antimony" by accident.**</span>
---
<!-- .slide: data-background-image="http://farm3.static.flickr.com/2506/5757000880_509440308e_z.jpg" -->
<span style="color:white">Ok. One more time. I promise. Here you go:</span>
---
<!-- .slide: data-background-video="https://i.imgur.com/wGK5hhG.mp4" data-background-video-loop="1" -->
