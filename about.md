---
layout: page
title: About
permalink: /about/
order: 2
---

I'm going to put my `about` page here, and if you want more details about my `about` page, you should check out my [*about* about page](https://xkcd.com/688/).

Other than that, see the details below, or get in touch!
